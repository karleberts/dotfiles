PATH=~/bin:"${PATH}":/usr/sbin:/sbin
export PATH
alias ls='ls --color -F'
alias psx='ps auxw | grep $1'
#alias ssh='/opt/bin/ssh'

#prompt magic
#%{\e[0;31m%}%m%{\e[0m%}
#CK="\[\033[0;30m\]"
CK="%{\\e[0;30m%}%m%{\\e[0m%}"
#DARK_GRAY="\[\033[1;30m\]"
DARK_GRAY="%{\\e[1;30m%}%m%{\\e[0m%}"
#LIGHT_GRAY="\[\033[0;37m\]"
LIGHT_GRAY="%{\\e[0;37m%}%m%{\\e[0m%}"
#BLUE="\[\033[0;34m\]"
BLUE="%{\\e[0;34m%}%m%{\\e[0m%}"
#LIGHT_BLUE="\[\033[1;34m\]"
LIGHT_BLUE="%{\\e[1;34m%}%m%{\\e[0m%}"
#GREEN="\[\033[0;32m\]"
GREEN="%{\\e[0;32m%}%m%{\\e[0m%}"
#LIGHT_GREEN="\[\033[1;32m\]"
LIGHT_GREEN="%{\\e[1;32m%}%m%{\\e[0m%}"
#CYAN="\[\033[0;36m\]"
CYAN="%{\\e[0;36m%}%m%{\\e[0m%}"
#LIGHT_CYAN="\[\033[1;36m\]"
LIGHT_CYAN="%{\\e[1;36m%}%m%{\\e[0m%}"
#RED="\[\033[0;31m\]"
RED="%{\\e[0;31m%}%m%{\\e[0m%}"
#LIGHT_RED="\[\033[1;31m\]"
LIGHT_RED="%{\\e[1;31m%}%m%{\\e[0m%}"
#PURPLE="\[\033[0;35m\]"
PURPLE="%{\\e[0;35m%}%m%{\\e[0m%}"
#LIGHT_PURPLE="\[\033[1;35m\]"
LIGHT_PURPLE="%{\\e[1;35m%}%m%{\\e[0m%}"
#BROWN="\[\033[0;33m\]"
BROWN="%{\\e[0;33m%}%m%{\\e[0m%}"
#YELLOW="\[\033[1;33m\]"
YELLOW="%{\\e[1;33m%}%m%{\\e[0m%}"
#WHITE="\[\033[1;37m\]"
WHITE="%{\\e[1;37m%}%m%{\\e[0m%}"
#DEFAULT_COLOR="\[\033[00m\]"
DEFAULT_COLOR="%{\\e[00m%}%m%{\\e[0m%}"

export PS1=`if [ \$? = 0 ];
    then
        #echo -e '$GREEN--( $LIGHT_CYAN\u$YELLOW@$LIGHT_CYAN\h$GREEN )--( $YELLOW\w$GREEN )-- :)\n--\$$DEFAULT_COLOR ';
		echo -e '%{\\e[0;32m%} (%{\\e[0m%}'
    else
        #echo -e '$LIGHT_RED--( $LIGHT_CYAN\u$YELLOW@$LIGHT_CYAN\h$LIGHT_RED )--( $YELLOW\w$LIGHT_RED )-- :(\n--\$$DEFAULT_COLOR ';
    fi; `
