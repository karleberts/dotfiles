-- [nfnl] Compiled from fnl/plugins/telescope.fnl by https://github.com/Olical/nfnl, do not edit.
local function _1_()
  vim.keymap.set("n", "<leader>ff", ":lua require('telescope.builtin').find_files()<CR>", {noremap = true})
  vim.keymap.set("n", "<leader>fg", ":lua require('telescope').extensions.live_grep_args.live_grep_args()<CR>", {noremap = true})
  vim.keymap.set("n", "<leader>fb", ":lua require('telescope.builtin').buffers()<CR>", {noremap = true})
  vim.keymap.set("n", "<leader>fh", ":lua require('telescope.builtin').help_tags()<CR>", {noremap = true})
  vim.keymap.set("n", "<leader>fc", ":lua require'telescope.builtin'.commands()<CR>", {noremap = true})
  return vim.keymap.set("n", "<leader>fr", ":lua require'telescope.builtin'.resume()<CR>", {noremap = true})
end
local function _2_()
  local telescope = require("telescope")
  telescope.setup({defaults = {file_ignore_patterns = {"node_modules"}}, pickers = {find_files = {find_command = {"rg", "--files", "--iglob", "!.git", "--hidden"}}}})
  return telescope.load_extension("live_grep_args")
end
return {"nvim-telescope/telescope.nvim", dependencies = {{"nvim-lua/popup.nvim"}, {"nvim-lua/plenary.nvim"}, {"nvim-telescope/telescope-live-grep-args.nvim"}}, init = _1_, config = _2_}
