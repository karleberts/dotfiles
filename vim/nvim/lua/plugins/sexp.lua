-- [nfnl] Compiled from fnl/plugins/sexp.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local function _1_()
  return core.assoc(vim.g, "sexp_filetypes", "clojure,scheme,lisp,timl,fennel,janet")
end
return {"guns/vim-sexp", dependencies = {{"tpope/vim-sexp-mappings-for-regular-people"}, {"tpope/vim-surround"}}, init = _1_}
