-- [nfnl] Compiled from fnl/plugins/mason-lspconfig.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local function _1_()
  local lsp = require("lspconfig")
  local mason_lspconfig = require("mason-lspconfig")
  local coq = require("coq")
  local on_attach
  local function _2_(_client, bufnr)
    vim.api.nvim_buf_set_keymap(bufnr, "n", "gd", "<Cmd>lua vim.lsp.buf.definition()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "K", "<Cmd>lua vim.lsp.buf.hover()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>ld", "<Cmd>lua vim.lsp.buf.declaration()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lt", "<cmd>lua vim.lsp.buf.type_definition()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lh", "<cmd>lua vim.lsp.buf.signature_help()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>ln", "<cmd>lua vim.lsp.buf.rename()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>le", "<cmd>lua vim.diagnostic.open_float()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lq", "<cmd>lua vim.diagnostic.setloclist()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lf", "<cmd>lua vim.lsp.buf.format()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lj", "<cmd>lua vim.diagnostic.goto_next()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lk", "<cmd>lua vim.diagnostic.goto_prev()<CR>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>la", ":lua vim.lsp.buf.code_action()<cr>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "v", "<leader>la", ":'<,'>:Telescope lsp_range_code_actions theme=cursor<cr>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lw", ":lua require('telescope.builtin').lsp_workspace_diagnostics()<cr>", {noremap = true})
    vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lr", ":lua require('telescope.builtin').lsp_references()<cr>", {noremap = true})
    return vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>li", ":lua require('telescope.builtin').lsp_implementations()<cr>", {noremap = true})
  end
  on_attach = _2_
  local default_config = coq.lsp_ensure_capabilities({on_attach = on_attach})
  mason_lspconfig.setup({ensure_installed = {"clojure_lsp", "intelephense", "ts_ls", "fennel_ls"}})
  lsp.clojure_lsp.setup(default_config)
  lsp.intelephense.setup(core.merge(default_config, {init_options = {licenceKey = "006XI4M04KYCZ33"}}))
  lsp.ts_ls.setup(default_config)
  lsp.fennel_ls.setup(default_config)
  --[[ (lsp.fennel_language_server.setup (core.merge default-config {:settings {:fennel {:diagnostics {:globals ["vim"]} :workspace {:library (vim.api.nvim_list_runtime_paths)}}}})) ]]
  return nil
end
return {"williamboman/mason-lspconfig.nvim", requires = {"mason", "coq-nvim", {"lspconfig"}}, config = _1_}
