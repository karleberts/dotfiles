-- [nfnl] Compiled from fnl/plugins/nerdtree.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local function _1_()
  vim.keymap.set("n", "<leader>tt", ":NERDTreeToggle<CR>", {noremap = true})
  vim.keymap.set("n", "<leader>tr", ":NERDTreeFind<CR>", {noremap = true})
  vim.cmd("autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif")
  core.assoc(vim.g, "NERDTreeShowHidden", 1)
  return core.assoc(vim.g, "NERDTreeIgnore", {"\\.git$[[dir]]"})
end
return {"preservim/nerdtree", dependencies = {"Xuyuanp/nerdtree-git-plugin"}, init = _1_}
