-- [nfnl] Compiled from fnl/plugins/coq-nvim.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local function _1_()
  return core.assoc(vim.g, "coq_conjure_autoregister", true)
end
return {"ms-jpq/coq_nvim", dependencies = {{"charleslambert/coq-conjure", init = _1_}}}
