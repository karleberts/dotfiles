-- [nfnl] Compiled from fnl/plugins/lspconfig.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local function define_signs(prefix)
  local error = (prefix .. "SignError")
  local warn = (prefix .. "SignWarn")
  local info = (prefix .. "SignInfo")
  local hint = (prefix .. "SignHint")
  vim.fn.sign_define(error, {text = "x", texthl = error})
  vim.fn.sign_define(warn, {text = "!", texthl = warn})
  vim.fn.sign_define(info, {text = "i", texthl = info})
  return vim.fn.sign_define(hint, {text = "?", texthl = hint})
end
local function _1_()
  return core.assoc(vim.g, "coq_settings", {auto_start = true})
end
local function _2_()
  if (vim.fn.has("nvim-0.6") == 1) then
    return define_signs("Diagnostic")
  else
    return define_signs("LspDiagnostics")
  end
end
return {"neovim/nvim-lspconfig", dependencies = {"coq_nvim"}, init = _1_, config = _2_}
