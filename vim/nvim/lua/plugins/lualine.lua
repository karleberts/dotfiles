-- [nfnl] Compiled from fnl/plugins/lualine.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local function lsp_connection()
  if vim.tbl_isempty(vim.lsp.buf_get_clients(0)) then
    return "\239\130\150"
  else
    return "\239\131\136"
  end
end
local function _2_()
  return require("lualine").setup({options = {theme = "solarized_dark", icons_enabled = true, section_separators = {"", ""}, component_separators = {"\239\145\138", "\239\144\184"}}, sections = {lualine_a = {}, lualine_b = {{"mode", {upper = true}}}, lualine_c = {{"FugitiveHead"}, {"filename", {filestatus = true, path = 1}}}, lualine_x = {{"diagnostics", {sections = {"error", "warn", "info", "hint"}, sources = {"nvim_lsp"}}}, {lsp_connection}, "location", "filetype"}, lualine_y = {"encoding"}, lualine_z = {}}, inactive_sections = {lualine_a = {}, lualine_b = {}, lualine_c = {{"filename", {filestatus = true, path = 1}}}, lualine_x = {}, lualine_y = {}, lualine_z = {}}})
end
return {"nvim-lualine/lualine.nvim", config = _2_}
