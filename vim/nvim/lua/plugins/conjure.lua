-- [nfnl] Compiled from fnl/plugins/conjure.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local function _1_()
  require("conjure.main").main()
  return require("conjure.mapping")["on-filetype"]()
end
local function _2_()
  core.assoc(vim.g, "conjure#mapping#doc_word", "K")
  core.assoc(vim.g, "conjure#client#clojure#nrepl#eval#auto_require", false)
  core.assoc(vim.g, "conjure#client#clojure#nrepl#connection#auto_repl#enabled", false)
  core.assoc(vim.g, "conjure#log#strip_ansi_escape_sequences_line_limit", 1)
  local function _3_()
    local buffer = vim.api.nvim_get_current_buf()
    vim.diagnostic.enable(false, {bufnr = buffer})
    if vim.g.conjure_baleia then
      return vim.g.conjure_baleia.automatically(buffer)
    else
      return nil
    end
  end
  return vim.api.nvim_create_autocmd({"BufWinEnter"}, {pattern = "conjure-log-*", callback = _3_})
end
return {"Olical/conjure", ft = {"clojure", "fennel"}, config = _1_, init = _2_}
