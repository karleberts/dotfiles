-- [nfnl] Compiled from fnl/plugins/neosolarized.fnl by https://github.com/Olical/nfnl, do not edit.
local function _1_()
  return vim.cmd("colorscheme NeoSolarized")
end
return {"overcache/NeoSolarized", init = _1_, lazy = false}
