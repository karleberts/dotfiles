-- [nfnl] Compiled from fnl/plugins/treesitter.fnl by https://github.com/Olical/nfnl, do not edit.
local function _1_()
  return require("nvim-treesitter").setup({highlight = {enable = true}, indent = {enable = true}, ensure_installed = {"clojure", "fennel", "json", "jsonc", "php", "rego", "typescript", "javascript", "python"}, rainbow = {enable = true, extended_mode = true, max_file_lines = nil}})
end
return {"nvim-treesitter/nvim-treesitter", build = ":TSUpdate", config = _1_}
