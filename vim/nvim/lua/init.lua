local _2afile_2a = "/home/karl/.config/nvim/fnl/init.fnl"
local _2amodule_name_2a = "nvim-config"
local _2amodule_2a
do
  package.loaded[_2amodule_name_2a] = {}
  _2amodule_2a = package.loaded[_2amodule_name_2a]
end
local _2amodule_locals_2a
do
  _2amodule_2a["aniseed/locals"] = {}
  _2amodule_locals_2a = (_2amodule_2a)["aniseed/locals"]
end
local autoload = (require("aniseed.autoload")).autoload
local a, nvim, packer, str, util = autoload("aniseed.core"), autoload("aniseed.nvim"), autoload("packer"), autoload("aniseed.string"), autoload("config.util")
do end (_2amodule_locals_2a)["a"] = a
_2amodule_locals_2a["nvim"] = nvim
_2amodule_locals_2a["packer"] = packer
_2amodule_locals_2a["str"] = str
_2amodule_locals_2a["util"] = util
nvim.set_keymap("n", "<space>", "<nop>", {noremap = true})
nvim.g.mapleader = " "
nvim.g.maplocalleader = ","
nvim.set_keymap("n", "<leader>wm", ":tab sp<cr>", {noremap = true})
nvim.set_keymap("n", "<leader>bn", ":bn", {noremap = true})
nvim.set_keymap("n", "<leader>ba", ":saveas", {noremap = true})
nvim.set_keymap("i", "jk", "<ESC>", {noremap = true})
nvim.set_keymap("i", "kj", "<ESC>", {noremap = true})
nvim.set_keymap("n", "gb", ":ls<CR>:b", {noremap = true})
nvim.set_keymap("t", "jk", "<C-\\><C-n>", {noremap = true})
nvim.set_keymap("t", "kj", "<C-\\><C-n>", {noremap = true})
nvim.ex.set("nowrap")
nvim.set_keymap("n", "<leader>\"", ":ConjureEval (user/start)<cr>", {noremap = true})
do
  local options = {background = "dark", completeopt = "menu,menuone,noselect", ignorecase = true, smartcase = true, clipboard = "unnamedplus"}
  for option, value in pairs(options) do
    a.assoc(nvim.o, option, value)
  end
end
vim.cmd("set relativenumber")
vim.cmd("set termguicolors")
vim.cmd("set nu rnu")
vim.cmd("set mouse=a")
vim.cmd("set autoindent")
vim.cmd("set expandtab")
vim.cmd("set tabstop=4")
vim.cmd("set softtabstop=4")
vim.cmd("set shiftwidth=4")
a.println("hihi")
require("config.plugin")
return _2amodule_2a