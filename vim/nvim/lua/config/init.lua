-- [nfnl] Compiled from fnl/config/init.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
vim.keymap.set("n", "<space>", "<nop>", {noremap = true})
vim.g.mapleader = " "
vim.g.maplocalleader = ","
vim.keymap.set("n", "<leader>wm", ":tab sp<cr>", {noremap = true})
vim.keymap.set("n", "<leader>bn", ":bn", {noremap = true})
vim.keymap.set("n", "<leader>ba", ":saveas", {noremap = true})
vim.keymap.set("i", "jk", "<ESC>", {noremap = true})
vim.keymap.set("i", "kj", "<ESC>", {noremap = true})
vim.keymap.set("n", "gb", ":ls<CR>:b", {noremap = true})
vim.keymap.set("t", "jk", "<C-\\><C-n>", {noremap = true})
vim.keymap.set("t", "kj", "<C-\\><C-n>", {noremap = true})
vim.api.nvim_win_set_option(0, "wrap", false)
vim.keymap.set("n", "<leader>\"", ":ConjureEval (user/start)<cr>", {noremap = true})
do
  local options = {background = "dark", completeopt = "menu,menuone,noselect", ignorecase = true, smartcase = true, clipboard = "unnamedplus"}
  for option, value in pairs(options) do
    core.assoc(vim.o, option, value)
  end
end
vim.cmd("set number")
vim.cmd("set termguicolors")
vim.cmd("set mouse=a")
vim.cmd("set autoindent")
vim.cmd("set expandtab")
vim.cmd("set tabstop=4")
vim.cmd("set softtabstop=4")
vim.cmd("set shiftwidth=4")
return {}
