-- [nfnl] Compiled from fnl/config/splash.fnl by https://github.com/Olical/nfnl, do not edit.
local core = require("nfnl.core")
local function get_method_node(node)
  local _1_ = node:type()
  if (_1_ == "program") then
    return nil
  elseif (_1_ == "method_declaration") then
    return node
  else
    local _ = _1_
    return get_method_node(node:parent())
  end
end
local function get_method_name(method_node)
  local children
  do
    local acc = {}
    for node, _ in method_node:iter_children() do
      table.insert(acc, node)
      acc = acc
    end
    children = acc
  end
  local ts = require("nvim-treesitter.ts_utils")
  local function _3_(node)
    return ("name" == node:type())
  end
  return core.first(ts.get_node_text(core.first(core.filter(_3_, children))))
end
local function get_current_method_name(current_node)
  local method_node = get_method_node(current_node)
  if method_node then
    return print(get_method_name(method_node))
  else
    return print("No method name found")
  end
end
local function get_class(node)
  local _5_ = node:type()
  if (_5_ == "program") then
    return nil
  elseif (_5_ == "class_declaration") then
    return node
  else
    local _ = _5_
    return get_class(node:parent())
  end
end
local function class_declaration__3ename_node(class_declaration_node)
  return class_declaration_node:named_child(0)
end
local website_path = "/Users/keberts/src/Website/"
local last_test_cmd = nil
local function run_last_test_21()
  if last_test_cmd then
    return vim.cmd(last_test_cmd)
  else
    return nil
  end
end
local function run_all_tests()
  local file_path = vim.fn.expand("%:p"):gsub(website_path, "")
  local test_cmd = table.concat({"Start", "docker exec -t", "app", "php artisan test", file_path}, " ")
  last_test_cmd = test_cmd
  return vim.cmd(test_cmd)
end
local function run_current_test_21()
  local ts = require("nvim-treesitter.ts_utils")
  local current_node = ts.get_node_at_cursor()
  local file_path = vim.fn.expand("%:p"):gsub(website_path, "")
  local method_name = get_method_name(get_method_node(current_node))
  local test_cmd = table.concat({"Start", "docker exec -t", "app", "php artisan test", file_path, ("--filter " .. method_name)}, " ")
  last_test_cmd = test_cmd
  return vim.cmd(test_cmd)
end
local function run_current_test()
  local _8_ = vim.bo.filetype
  if (_8_ == "php") then
    return run_current_test_21()
  else
    local _ = _8_
    return print("I don't know how to run a test here")
  end
end
local function run_last_test()
  local _10_ = vim.bo.filetype
  if (_10_ == "php") then
    return run_last_test_21()
  else
    local _ = _10_
    return print("I don't know how to run a test here")
  end
end
vim.api.nvim_create_user_command("RunAllTests", run_all_tests, {})
vim.api.nvim_create_user_command("RunCurrentTest", run_current_test, {})
vim.api.nvim_create_user_command("RunLastTest", run_last_test, {})
local phpcmdgroup = vim.api.nvim_create_augroup("PHP Specific settings", {clear = true})
local function _12_()
  return vim.keymap.set("n", "<localleader>tn", ":RunAllTests<cr>", {buffer = true, noremap = true})
end
vim.api.nvim_create_autocmd("FileType", {group = phpcmdgroup, pattern = {"php"}, callback = _12_})
local function _13_()
  return vim.keymap.set("n", "<localleader>tc", ":RunCurrentTest<cr>", {buffer = true, noremap = true})
end
vim.api.nvim_create_autocmd("FileType", {group = phpcmdgroup, pattern = {"php"}, callback = _13_})
local function _14_()
  return vim.keymap.set("n", "<localleader>tl", ":RunLastTest<cr>", {buffer = true, noremap = true})
end
vim.api.nvim_create_autocmd("FileType", {group = phpcmdgroup, pattern = {"php"}, callback = _14_})
return {}
