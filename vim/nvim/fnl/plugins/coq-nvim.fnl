(local core (require :nfnl.core))

{1 :ms-jpq/coq_nvim
 :dependencies [{1 :charleslambert/coq-conjure
                 :init (fn [] (core.assoc vim.g :coq_conjure_autoregister true))}]}
