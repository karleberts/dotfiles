(local core (require :nfnl.core))

{1 :williamboman/mason-lspconfig.nvim
:requires [:mason :coq-nvim {1 :lspconfig}]
:config (fn []
          (let [lsp (require :lspconfig)
                    mason-lspconfig (require :mason-lspconfig)
                    coq (require :coq)
                    on_attach (fn [_client bufnr]
                                (do
                                  (vim.api.nvim_buf_set_keymap bufnr :n :gd "<Cmd>lua vim.lsp.buf.definition()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :K "<Cmd>lua vim.lsp.buf.hover()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>ld "<Cmd>lua vim.lsp.buf.declaration()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>lt "<cmd>lua vim.lsp.buf.type_definition()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>lh "<cmd>lua vim.lsp.buf.signature_help()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>ln "<cmd>lua vim.lsp.buf.rename()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>le "<cmd>lua vim.diagnostic.open_float()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>lq "<cmd>lua vim.diagnostic.setloclist()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>lf "<cmd>lua vim.lsp.buf.format()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>lj "<cmd>lua vim.diagnostic.goto_next()<CR>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>lk "<cmd>lua vim.diagnostic.goto_prev()<CR>" {:noremap true})
                                  ;telescope
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>la ":lua vim.lsp.buf.code_action()<cr>" {:noremap true})
                                  ;(vim.api.nvim_buf_set_keymap bufnr :n :<leader>la ":lua require('telescope.builtin').lsp_code_actions(require('telescope.themes').get_cursor())<cr>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :v :<leader>la ":'<,'>:Telescope lsp_range_code_actions theme=cursor<cr>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>lw ":lua require('telescope.builtin').lsp_workspace_diagnostics()<cr>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>lr ":lua require('telescope.builtin').lsp_references()<cr>" {:noremap true})
                                  (vim.api.nvim_buf_set_keymap bufnr :n :<leader>li ":lua require('telescope.builtin').lsp_implementations()<cr>" {:noremap true})))
                    default-config (coq.lsp_ensure_capabilities {:on_attach on_attach})]
            (mason-lspconfig.setup {:ensure_installed ["clojure_lsp"
                                                       "intelephense"
                                                       "ts_ls"
                                                       "fennel_ls"]})
            (lsp.clojure_lsp.setup default-config) ; cmp-nvim-lsp chokes on clojure-lsp's output from stonehenge
            (lsp.intelephense.setup (core.merge default-config
                                                {:init_options {:licenceKey "006XI4M04KYCZ33"}}))
            (lsp.ts_ls.setup default-config)
            (lsp.fennel_ls.setup default-config)
            (comment (lsp.fennel_language_server.setup (core.merge default-config
                                                          {:settings {:fennel {:workspace {:library (vim.api.nvim_list_runtime_paths)}
                                                          :diagnostics {:globals ["vim"]}}}})))))}
