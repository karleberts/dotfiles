(local core (require :nfnl.core))

(fn define-signs
      [prefix]
      (let [error (.. prefix "SignError")
                  warn  (.. prefix "SignWarn")
                  info  (.. prefix "SignInfo")
                  hint  (.. prefix "SignHint")]
        (vim.fn.sign_define error {:text "x" :texthl error})
        (vim.fn.sign_define warn  {:text "!" :texthl warn})
        (vim.fn.sign_define info  {:text "i" :texthl info})
        (vim.fn.sign_define hint  {:text "?" :texthl hint})))

{1 :neovim/nvim-lspconfig
 :dependencies [:coq_nvim]
 :init (fn [] (core.assoc vim.g :coq_settings {:auto_start true}))
 :config (fn []
           (if (= (vim.fn.has "nvim-0.6") 1)
              (define-signs "Diagnostic")
              (define-signs "LspDiagnostics")))}
