{1 :nvim-telescope/telescope.nvim
 :dependencies [{1 :nvim-lua/popup.nvim}
                {1 :nvim-lua/plenary.nvim}
                {1 :nvim-telescope/telescope-live-grep-args.nvim}]
 :init (fn []
(vim.keymap.set :n :<leader>ff ":lua require('telescope.builtin').find_files()<CR>" {:noremap true})
(vim.keymap.set :n :<leader>fg ":lua require('telescope').extensions.live_grep_args.live_grep_args()<CR>" {:noremap true})
(vim.keymap.set :n :<leader>fb ":lua require('telescope.builtin').buffers()<CR>" {:noremap true})
(vim.keymap.set :n :<leader>fh ":lua require('telescope.builtin').help_tags()<CR>" {:noremap true})
(vim.keymap.set :n :<leader>fc ":lua require'telescope.builtin'.commands()<CR>" {:noremap true})
(vim.keymap.set :n :<leader>fr ":lua require'telescope.builtin'.resume()<CR>" {:noremap true})
         )
 :config (fn []
           (let [telescope (require :telescope)]
             ((. telescope "setup") {:defaults {:file_ignore_patterns ["node_modules"]}
                                  :pickers {:find_files {:find_command ["rg" "--files" "--iglob" "!.git" "--hidden"]}}})
             ((. telescope "load_extension") :live_grep_args)))}
