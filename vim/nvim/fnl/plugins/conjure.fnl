(local core (require :nfnl.core))

{1 :Olical/conjure
 :ft ["clojure" "fennel"]
 :config (fn []
           ((. (require :conjure.main) :main))
           ((. (require :conjure.mapping) :on-filetype)))
 :init (fn []
         (core.assoc vim.g "conjure#mapping#doc_word" "K")
         (core.assoc vim.g "conjure#client#clojure#nrepl#eval#auto_require" false)
         (core.assoc vim.g "conjure#client#clojure#nrepl#connection#auto_repl#enabled" false)
         (core.assoc vim.g "conjure#log#strip_ansi_escape_sequences_line_limit" 1)
         (vim.api.nvim_create_autocmd [:BufWinEnter]
                               {:pattern "conjure-log-*"
                               :callback (fn []
                                           (local buffer (vim.api.nvim_get_current_buf))
                                           (vim.diagnostic.enable false {:bufnr buffer})
                                           (when vim.g.conjure_baleia
                                             (vim.g.conjure_baleia.automatically buffer)))}))

}
