(local core (require :nfnl.core))

{1 :guns/vim-sexp
 :dependencies [{1 :tpope/vim-sexp-mappings-for-regular-people}
                {1 :tpope/vim-surround}]
 :init (fn []
          (core.assoc vim.g :sexp_filetypes "clojure,scheme,lisp,timl,fennel,janet"))}
