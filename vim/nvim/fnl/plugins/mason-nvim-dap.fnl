(local core (require :nfnl.core))

{1 :jay-babu/mason-nvim-dap.nvim
:requires [:mason :nvim-dap]
:dependencies [:nvim-telescope/telescope.nvim :nvim-telescope/telescope-dap.nvim]
:config (fn []
          (let [dap (require :dap)
                ;; dap-ui (require :dapui)
                nvim-dap (require :mason-nvim-dap)
                telescope (require :telescope)
                widgets (require "dap.ui.widgets")
                scopes (widgets.sidebar widgets.scopes {} :vsplit)
                frames (widgets.sidebar widgets.frames {:height 10} "belowright split")
                repl (require "dap.repl")]
            (nvim-dap.setup {:ensure_installed [:php]
                            :handlers {1 (fn [config]
                                           (nvim-dap.default_setup config))
                                       :php (fn [config]
                                              (core.assoc config :configurations [{:name "PHP: Listen for Xdebug"
                                                                                         :port 9002
                                                                                         :hostname "0.0.0.0"
                                                                                         :type :php
                                                                                         :request :launch
                                                                                         :log true
                                                                                         :pathMappings {"/var/www" "${workspaceFolder}"}}])
                                              (nvim-dap.default_setup config))}})
            ;; (dap-ui.setup)
            (telescope.load_extension :dap)

            (vim.keymap.set :n "<F5>" dap.continue)
            ; (vim.keymap.set :n "<F7>" dap-ui.toggle {:desc "Debug: See last session result."})
            (vim.keymap.set :n "<F10>" dap.step_over)
            (vim.keymap.set :n "<F11>" dap.step_into)
            (vim.keymap.set :n "<F12>" dap.step_out)
            (vim.keymap.set :n "<localleader>db" dap.toggle_breakpoint)
            (vim.keymap.set :n "<localleader>dr" dap.restart)
            (vim.keymap.set :n "<localleader>dt" dap.terminate)
            (vim.keymap.set :n "<localleader>da" (fn [] (repl.toggle {} "belowright split")))
            (vim.keymap.set :n "<localleader>ds" scopes.toggle)
            (vim.keymap.set :n "<localleader>du" frames.toggle)
            (vim.keymap.set :n "<localleader>dh" widgets.hover)

            (vim.keymap.set :n "<localleader>df" ":Telescope dap frames<cr>" {:noremap true})
            (vim.keymap.set :n "<localleader>dbl" ":Telescope dap list_breakpoints<cr>")

            

            ))}
