(local core (require :nfnl.core))

{1 :preservim/nerdtree
 :dependencies [:Xuyuanp/nerdtree-git-plugin]
 :init (fn []
         (vim.keymap.set :n :<leader>tt ":NERDTreeToggle<CR>" {:noremap true})
         (vim.keymap.set :n :<leader>tr ":NERDTreeFind<CR>" {:noremap true})

         ;close the tab if nerdtree is the only window remaining in it.
         (vim.cmd "autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif")

         (core.assoc vim.g :NERDTreeShowHidden 1)
         (core.assoc vim.g :NERDTreeIgnore ["\\.git$[[dir]]"]))}
