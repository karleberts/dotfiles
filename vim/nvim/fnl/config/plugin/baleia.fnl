(module config.plugin.lspconfig
  {autoload {nvim aniseed.nvim
             baleia baleia}})

(defn config
  []
  (error "foooo")
  (baleia.setup {:line_starts_at 3})

  (local augroup (vim.api.nvim_create_augroup "ConjureBaleia" {:clear true}))

  (vim.api.nvim_create_user_command "BaleiaColorize"
                                    (fn []
                                      (vim.g.conjure_baleia.once (vim.api.nvim_get_current_buf)))
                                    {:bang true})

  (vim.api.nvim_create_user_command "BaleiaLogs"
                                    vim.g.conjure_baleia.logger.show
                                    {:bang true}))
(def packer-args {:config config})
