(module config.plugin.vdebug
  {autoload {nvim aniseed.nvim}})

(set nvim.g.vdebug_options {:port 9002})
