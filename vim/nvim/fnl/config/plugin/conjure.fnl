(module config.plugin.conjure
  {autoload {nvim aniseed.nvim}})

(set nvim.g.conjure#mapping#doc_word "K")
(set nvim.g.conjure#client#clojure#nrepl#eval#auto_require false)
(set nvim.g.conjure#client#clojure#nrepl#connection#auto_repl#enabled false)

(defn config
  []
  ((. (require :conjure.main) :main))
  ((. (require :conjure.mapping) :on-filetype)))

(defn init
  []
  (vim.g :conjure#log#strip_ansi_escape_sequences_line_limit 1)
  (vim.api.nvim_create_autocmd [:BufWinEnter]
                               {:pattern "conjure-log-*"
                                :callback (fn []
                                            (local buffer (vim.api.nvim_get_current_buf))
                                            (vim.diagnostic.enable false {:bufnr buffer})
                                            (when vim.g.conjure_baleia
                                              (vim.g.conjure_baleia.automatically buffer)))}))
