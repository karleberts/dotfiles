(module config.plugin.mason-nvim-dap
  {autoload {dap mason-nvim-dap
             d dap}})


(set d.adapters.php {:type :executable
                     :command (vim.fn.exepath :php-debug-adapter)})
(set d.configurations.php [{:type :php
                            :request :launch
                            :name "foo listen"
                            :port 9003
                            :log true
                            :pathMappings {"/var/www/" "/home/karl/src/Website/"}}
                           {:type :php
                            :request :launch
                            :name "foo2 listen"
                            :log true
                            :port 9003}])

(dap.setup {:ensure_installed ["php-debug-adapter"
                               "js-debug-adapter"]})
