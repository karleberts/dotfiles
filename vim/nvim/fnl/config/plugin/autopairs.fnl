(module config.plugin.autopairs
    {autoload {autopairs nvim-autopairs}})

(autopairs.setup
    {:check_ts true})
