(module config.plugin.treesitter
  {autoload {treesitter nvim-treesitter}})

(treesitter.setup {:highlight {:enable true}
                   :indent {:enable true}
                   :ensure_installed ["clojure" "fennel" "json" "jsonc" "php"
                                      "rego" "typescript" "javascript" "python"]
                   :rainbow {:enable true
                             :extended_mode true
                             :max_file_lines nil}})
