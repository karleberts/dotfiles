(module config.plugin.devcontainer
  {require {devcontainer devcontainer}})

(devcontainer.setup {:generate_commands true
                     :always_mount ["/home/karl/bin/nvim" "/usr/bin/vim"]})
