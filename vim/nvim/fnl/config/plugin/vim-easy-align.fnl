(module config.plugin.vim-easy-align
  {autoload {nvim aniseed.nvim}})

(vim.cmd "xmap ga <Plug>(EasyAlign)")
(vim.cmd "nmap ga <Plug>(EasyAlign)")
(vim.cmd "map <C-a> vi{ga-<Space>")
(vim.cmd "map <C-A> vi[ga-<Space>")

