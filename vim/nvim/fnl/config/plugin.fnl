(module config.plugin
  {autoload {nvim aniseed.nvim
             a aniseed.core
             util config.util
             packer packer}})

(defn- safe-require-plugin-config [name]
  (let [(ok? val-or-err) (pcall require (.. :config.plugin. name))]
    (if ok?
      val-or-err
      (print (.. "config error: " val-or-err)))))

(defn- use [...]
  "Iterates through the arguments as pairs and calls packer's use function for
  each of them. Works around Fennel not liking mixed associative and sequential
  tables as well."
  (let [pkgs [...]]
    (packer.startup
      (fn [use]
        (for [i 1 (a.count pkgs) 2]
          (let [name (. pkgs i)
                opts (. pkgs (+ i 1))
                plugin-config (or (-?> (. opts :mod) (safe-require-plugin-config)) {})
                packer-args (. plugin-config :packer-args)]
            (use (-> opts
                     (a.assoc 1 name)
                     (a.merge packer-args))))))))
  nil)

;;; plugins managed by packer
;;; :mod specifies namespace under plugin directory

(use
  ;; plugin Manager
  :wbthomason/packer.nvim {}
  ;; nvim config and plugins in Fennel
  :Olical/aniseed {:branch :develop}

  ;; theme
  :overcache/NeoSolarized {}
  :kyazdani42/nvim-web-devicons {}
  :m00qek/baleia {:mod :baleia}

  ;; status line
  :nvim-lualine/lualine.nvim {:mod :lualine}

  ;; file searching
  :nvim-telescope/telescope.nvim {:requires [:nvim-lua/popup.nvim
                                             :nvim-lua/plenary.nvim]
                                  :mod :telescope}
  :preservim/nerdtree {:mod :nerdtree
                       :requires [:Xuyuanp/nerdtree-git-plugin ]}

  ;; repl tools
  :Olical/conjure {:branch :master
                   :mod :conjure
                   :requires [:m00qek/baleia]}

  ;;debugger
  ;:vim-vdebug/vdebug {:mod :vdebug}

  ;; task runner
  :radenling/vim-dispatch-neovim {:requires [:tpope/vim-dispatch]}

  ;;devcontainer
  ;esensar/nvim-dev-container {:mod :devcontainer
  ;                             :requires [:nvim-treesitter/nvim-treesitter]}

  ;; sexp
  :guns/vim-sexp {:mod :sexp}
  :tpope/vim-sexp-mappings-for-regular-people {}
  :tpope/vim-repeat {}
  :tpope/vim-surround {}

  ;; parsing system
  :nvim-treesitter/nvim-treesitter {:run ":TSUpdate"
                                    :mod :treesitter}

  ;; autocomplete
  :ms-jpq/coq_nvim {:mod :coq_nvim
                    :requires [:charleslambert/coq-conjure]}

  ;; lsp
  :williamboman/mason.nvim {:mod :mason}
  :williamboman/mason-lspconfig.nvim {:mod :mason-lspconfig
                                      :requires [:williamboman/mason.nvim
                                                 :ms-jpq/coq_nvim]}
  :neovim/nvim-lspconfig {:mod :lspconfig}

  ;; dap
  :mfussenegger/nvim-dap {:requires [:williamboman/mason.nvim]}
  :jay-babu/mason-nvim-dap.nvim {:requires [:williamboman/mason.nvim
                                            :mfussenegger/nvim-dap]
                                 :mod :mason-nvim-dap}

  ;:hrsh7th/nvim-cmp {:requires [:hrsh7th/cmp-buffer
                                ;:hrsh7th/cmp-nvim-lsp
  ;                              :PaterJason/cmp-conjure
  ;                              :hrsh7th/cmp-vsnip
  ;                              :hrsh7th/vim-vsnip
  ;                              ]
  ;                   :mod :cmp}
  ;:windwp/nvim-autopairs {:mod :autopairs}

  )
