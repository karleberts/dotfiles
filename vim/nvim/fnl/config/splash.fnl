(local core (require :nfnl.core))

(fn get-method-node [node]
  (case (node:type)
    :program nil
    :method_declaration node
    _ (get-method-node (node:parent))))

(fn get-method-name [method-node]
  (let [children (accumulate [acc {}
                              node _ (method-node:iter_children)]
                   (do (table.insert acc node) acc))
        ts (require :nvim-treesitter.ts_utils)]
    (->> children
       (core.filter (fn [node] (= :name (node:type))))
       (core.first)
       (ts.get_node_text)
       (core.first))))

(fn get-current-method-name [current-node]
  (let [method-node (get-method-node current-node)]
    (if method-node
      (print (get-method-name method-node))
      (print "No method name found"))))

(fn get-class [node]
  (case (node:type)
    :program nil
    :class_declaration node
    _ (get-class (node:parent))))

(fn class-declaration->name-node [class-declaration-node]
    (class-declaration-node:named_child 0))

(local website-path "/Users/keberts/src/Website/")

(var last-test-cmd nil)

(fn run-last-test! []
  (when last-test-cmd
    (vim.cmd last-test-cmd)))

(fn run-all-tests []
  (let [file-path (-> (vim.fn.expand "%:p")
                      (: :gsub website-path ""))
        test-cmd (table.concat ["Start"
                                "docker exec -t"
                                "app"
                                "php artisan test"
                                file-path] " ")]
    (set last-test-cmd test-cmd)
    (vim.cmd test-cmd)))

(fn run-current-test! []
  (let [ts (require :nvim-treesitter.ts_utils)
        current-node (ts.get_node_at_cursor)
        file-path (-> (vim.fn.expand "%:p")
                      (: :gsub website-path ""))
        method-name (-> current-node
                        (get-method-node)
                        (get-method-name))
        test-cmd (table.concat ["Start"
                                "docker exec -t"
                                "app"
                                "php artisan test"
                                file-path
                                (.. "--filter " method-name)] " ")]
    (set last-test-cmd test-cmd)
    (vim.cmd test-cmd)))

(fn run-current-test []
  (case vim.bo.filetype
    :php (run-current-test!)
    _ (print "I don't know how to run a test here")))

(fn run-last-test []
  (case vim.bo.filetype
    :php (run-last-test!)
    _ (print "I don't know how to run a test here")))

(vim.api.nvim_create_user_command :RunAllTests run-all-tests {})
(vim.api.nvim_create_user_command :RunCurrentTest run-current-test {})
(vim.api.nvim_create_user_command :RunLastTest run-last-test {})

(local phpcmdgroup (vim.api.nvim_create_augroup "PHP Specific settings" {:clear true}))
(vim.api.nvim_create_autocmd :FileType {:group phpcmdgroup
                                        :pattern ["php"]
                                        :callback (fn []
                                                    (vim.keymap.set :n "<localleader>tn" ":RunAllTests<cr>" {:buffer true :noremap true}))
                                        :group phpcmdgroup})

(vim.api.nvim_create_autocmd :FileType {:group phpcmdgroup
                                        :pattern ["php"]
                                        :callback (fn []
                                                    (vim.keymap.set :n "<localleader>tc" ":RunCurrentTest<cr>" {:buffer true :noremap true}))
                                        :group phpcmdgroup})

(vim.api.nvim_create_autocmd :FileType {:group phpcmdgroup
                                        :pattern ["php"]
                                        :callback (fn []
                                                    (vim.keymap.set :n "<localleader>tl" ":RunLastTest<cr>" {:buffer true :noremap true}))
                                        :group phpcmdgroup})

{}
