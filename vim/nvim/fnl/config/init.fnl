(local core (require :nfnl.core))

;generic mapping leaders configuration
(vim.keymap.set :n :<space> :<nop> {:noremap true})
(set vim.g.mapleader " ")
(set vim.g.maplocalleader ",")

;; Spacemacs style leader mappings.
(vim.keymap.set :n :<leader>wm ":tab sp<cr>" {:noremap true})
;; new buffer
(vim.keymap.set :n :<leader>bn ":bn" {:noremap true})
(vim.keymap.set :n :<leader>ba ":saveas" {:noremap true})

;; Insert mode: :jk as Escape short cut
(vim.keymap.set :i :jk "<ESC>" {:noremap true})
(vim.keymap.set :i :kj "<ESC>" {:noremap true})

;;normal mode :gb to list buffers
(vim.keymap.set :n :gb ":ls<CR>:b" {:noremap true})

;;term mode :jk to return to normal mode
(vim.keymap.set :t :jk "<C-\\><C-n>" {:noremap true})
(vim.keymap.set :t :kj "<C-\\><C-n>" {:noremap true})

;don't wrap lines
(vim.api.nvim_win_set_option 0 "wrap" false)

;;*clj specific bindings*
(vim.keymap.set :n "<leader>\"" ":ConjureEval (user/start)<cr>" {:noremap true})

;sets a nvim global options
(let [options
      {
       :background "dark"
       ;settings needed for compe autocompletion
       :completeopt "menu,menuone,noselect"
       ;case insensitive search
       :ignorecase true
       ;smart search case
       :smartcase true
       ;shared clipboard with linux
       :clipboard "unnamedplus"}]
  (each [option value (pairs options)]
    (core.assoc vim.o option value)))
;;(vim.cmd "set relativenumber")
(vim.cmd "set number")
(vim.cmd "set termguicolors")
;;(vim.cmd "set nu rnu")
(vim.cmd "set mouse=a")
(vim.cmd "set autoindent")
(vim.cmd "set expandtab")
(vim.cmd "set tabstop=4")
(vim.cmd "set softtabstop=4")
(vim.cmd "set shiftwidth=4")
{}
