function! RunPerlLint() range

	let filename = expand("%:p")
	set previewheight=3
    let ln = line('.')
    echo ln

"    let src = tempname()
    let dst = "LinterWindow"
    let hilight = tempname()

    " put current buffer's content in a temp file
"    silent execute "normal:w!" . src . "\<CR>"
    silent execute ":bot pedit! " . dst

    " change to preview window
    wincmd P
    setlocal buftype=nofile
    setlocal noswapfile
    setlocal syntax=none
    setlocal bufhidden=delete

    " replace current buffer with jslint's output
    " I redirect STDERR to the pw and stdout (which lines to higlight) to a temp file
    silent execute ':%! /usr/local/vimReview/vimPerlCriticReview.pl -l ' . ln . ' -f '. filename .' 1> ' . hilight

    " read in that temp file that contained lines to highlight
    let x = system('cat ' . hilight)
	let foo = len(x) / 8

    if strlen(x) > 3
        call matchadd('Error','\%1l')
		silent execute ':file! {' . foo . ' Errors. Closest error to cursor displayed above}'
    else
		call matchadd('Success','\%1l')
    endif

    " change back to the source buffer
    wincmd p

    " Clear old highlights and add the new ones
    " done after we are out of the preview window
    call clearmatches()
    call matchadd('Error',x)
endfunction

"map <F3> :call RunPerlLint()<CR>
