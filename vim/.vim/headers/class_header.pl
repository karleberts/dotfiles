
#################################################################################
# Package: PACKAGE_NAME_HERE
#
# PACKAGE_DESCRIPTION_HERE
#
# About: Extends
#
# If this package extends another package, replace this text with the package name in 
#	angle brackets. For example, if this extended Switchvox::Database, then put:
#
# <Switchvox::Database>
#
# If this doesn't extend anything, then delete all of this.
#
# Parameters:
#
# param1Name - this is a prameter
# param2Name - *[optional]* this is an optional parameter
# param3Name - *[deprecated]* this is a deprecated parameter
#
# Returns:
#
# DESC_OF_RETURN_VALUE
#################################################################################
