
/********************************************************************************
* Class: CLASS_NAME_HERE
*
* CLASS_DESCRIPTION_HERE
*
* About: Extends
*
* If this class extends another class, replace this text with the class name in 
*	angle brackets. For example, if this extended switchvox.controls.base, then put:
*
* <switchvox.controls.base>
*
* If this doesn't extend anything, then delete all of this.
*
* Parameters:
*
* param1Name - this is a prameter
* param2Name - *[optional]* this is an optional parameter
* param3Name - *[deprecated]* this is a deprecated parameter
*
* Returns:
*
* DESC_OF_RETURN_VALUE
********************************************************************************/
