//======================================================
// @author: Ryan Navaroli
// @class: wms_api_widgets
// @desc: methods for interacting with widget settings
// @public: getList, add, update, updateRows, remove
//=======================================================
