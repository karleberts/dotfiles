#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# .config/i3pystatus/config.py
#
import subprocess
import os
import os.path

from i3pystatus import Status

status = Status()

status.register("clock",
    format="  -  %a %d-%m-%Y ",
    color='#61AEEE',
    interval=120,)

status.register("clock",
    format="  %H:%M",
    color='#C678DD',
    interval=30,)

status.register("mem",
    color="#999999",
    warn_color="#E5E500",
    alert_color="#FF1919",
    format="  {avail_mem}/{total_mem} GB",
    divisor=1073741824,)

status.register("load",
    format="  {tasks}",)

status.register("load",
    format="  {avg5}/{avg15}",)

status.register("cpu_usage",
    format="  {usage}%",)

status.run()
