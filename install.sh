#!/bin/bash

#make sure the required programs are installed
type git >/dev/null 2>&1 || { echo >&2 "I require git but it's not installed.  Aborting."; exit 1; }
type npm >/dev/null 2>&1 || { echo >&2 "I require npm but it's not installed.  Aborting."; exit 1; }
type cmake >/dev/null 2>&1 || { echo >&2 "I require cmake but it's not installed.  Aborting."; exit 1; }

DIR="$( cd -P "$( dirname "$0" )" && pwd )"

function prompt_overwrite {
	echo -n "(y/n) "
	read RESPONSE
	if [ "$RESPONSE" = "y" ]; then
		#return 0 for success status (true)
		return 0
	elif [ "$RESPONSE" = "n" ]; then
		#return 1 for fail status (false)
		return 1
	else
		echo "Must enter y or n."
		prompt_overwrite
		return $?
	fi
}
function do_install {
	source_file="$(pwd)/$1"
	target_filename=$(eval echo "$2")
	echo "Installing symlink for $source_file"
	make_link=1
	if [ -e $target_filename ]; then
		if [ -L $target_filename ]; then
			make_link=0
			link_target=$(readlink $target_filename)
			if [ $link_target != $source_file ]; then
				#get link target and prompt user to overwrite IFF target is not already a link to source_file
				echo "$target_filename is a symlink to $link_target. Overwrite?"
				if prompt_overwrite; then
					echo "Unlinking $link_target"
					unlink $link_target
					make_link=1
				else
					make_link=0
				fi
			else
				echo "$target_filename already links to $source_file"
			fi
		elif [ -f $target_filename ] || [ -d $target_filename ] ; then
			echo "$target_filename is a file/dir, attempting backup."
			make_link=0
			#check if backup exists, prompt to overwrite if it does
			if [ -e "$target_filename.bak" ]; then
				echo "$target_filename.bak already exists. Overwrite with $target_filename?"
				if prompt_overwrite; then
					#delete the .bak and mv $target_filename
					rm -rf $target_filename.bak
					mv $target_filename $target_filename.bak
					make_link=1
				else
					echo "Would you like to overwrite $source_file without backing up?"
					if prompt_overwrite; then
						make_link=1
					else
						make_link=0
					fi
				fi
			else
				echo "Backing up $target_filename"
				mv $target_filename "$target_filename.bak"
				make_link=1
			fi
		fi
	fi
	if [ $make_link -eq 1 ]; then
		echo "Creating symlink to $source_file"
		mkdir -p $(dirname "${target_filename}")
		ln -s $source_file $target_filename
	else
		echo "Skipping $source_file"
	fi
	echo ""
}

#loop through all the folders and install all the configs in the _targets_ files
for dir in `ls $DIR`; do
	if [ -d $DIR/$dir ]; then
		cd $DIR/$dir
		if [ -e _targets_ ]; then
			while read line <&9; do
				do_install $line
			done 9< _targets_
		fi
	fi
done
